from django.urls import path
from roofsensors import views
from dashboard import views as dashboard_views
urlpatterns = [
    path('data/', views.SensorDataView.as_view()),
    path('data/ambient/', views.AmbientSensorDataView.as_view()),
    path('', dashboard_views.DashboardView.as_view()),
    path('export/', dashboard_views.CSVExport.as_view()),
]
