from django.db import models

class ExperimentData(models.Model):
    recieved_at = models.DateTimeField(auto_now=True)
    green_roof_top = models.FloatField(null=True)
    green_roof_bottom = models.FloatField(null=True)
    control_top = models.FloatField(null=True)
    green_roof_middle = models.FloatField(null=True)
    control_bottom = models.FloatField(null=True)

    surface_heat_difference = models.FloatField(null=True) # control top - green middle
    bottom_difference = models.FloatField(null=True) # control bottom - green bottom

    moist_s0 = models.IntegerField(null=True)
    moist_s1 = models.IntegerField(null=True)

class AmbientData(models.Model):
    recorded_at = models.DateTimeField(null=True)
    device_name = models.CharField(max_length=100, null=True)
    humidity = models.FloatField(null=True)
    illumination = models.IntegerField(null=True)
    temperature = models.FloatField(null=True)
