from django.core.management.base import BaseCommand
from roofsensors.models import ExperimentData, AmbientData
from datetime import datetime, timedelta

class Command(BaseCommand):
    def handle(self, *kwargs, **options):
        ambient = AmbientData.objects.all()
        for a in ambient:
            a.recorded_at = a.recorded_at - timedelta(hours=1)
            a.save()
