from django.core.management.base import BaseCommand
from roofsensors.models import ExperimentData, AmbientData
from datetime import datetime, timedelta

class Command(BaseCommand):
    def handle(self, *kwargs, **options):
        ambient = AmbientData.objects.all()
        for a in ambient:
            if a.red_one_humidity:
                a.device_name = "Red 1"
                a.humidity = a.red_one_humidity
                a.illumination = a.red_one_illumination
                a.temperature = a.red_one_temperature

            if a.red_two_humidity:
                a.device_name = "Red 2"
                a.humidity = a.red_two_humidity
                a.illumination = a.red_two_illumination
                a.temperature = a.red_two_temperature

            if a.red_three_humidity:
                a.device_name = "Red 3"
                a.humidity = a.red_three_humidity
                a.illumination = a.red_three_illumination
                a.temperature = a.red_three_temperature
            a.save()
