import csv, pytz, codecs
from datetime import timedelta, datetime

from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic import TemplateView
from django.utils import timezone
from django.utils.timezone import activate
from django.utils.dateparse import parse_date
from django.contrib.humanize.templatetags import humanize

from roofsensors.models import ExperimentData, AmbientData

class DashboardView(TemplateView):
    template_name = "dashboard/index.html"

    def status_for_time(self, seconds, interval):
        if seconds <= interval: #4 mins
            return "success"
        elif seconds <= interval*2: # 8mins
            return "warning"
        return "danger"

    def get(self, request):

        activate(pytz.timezone('Europe/London'))

        # Date range
        now = timezone.now()
        start = now - timedelta(hours=24)
        data = {}

        # Experiment Data
        experiment_data = ExperimentData.objects.filter(recieved_at__range=[start, now])
        latest = ExperimentData.objects.all().last()
        if latest:
            data['latest'] = latest
            data['latest_date'] = humanize.naturaltime(latest.recieved_at)
            received_delta = (now - latest.recieved_at).seconds
            data['experiment_status'] = self.status_for_time(received_delta, 200)

        data['green_roof_middle'] = experiment_data.values('recieved_at','green_roof_middle')
        data['green_roof_top'] = experiment_data.values('recieved_at','green_roof_top')
        data['control_top'] = experiment_data.values('recieved_at','control_top')

        # Ambient Data
        ambient_data = AmbientData.objects.filter(recorded_at__range=[start, now]).order_by('recorded_at')
        ambient_latest = AmbientData.objects.all().last()
        if ambient_latest:
            data['ambient_latest'] = ambient_latest
            data['ambient_latest_date'] = humanize.naturaltime(ambient_latest.recorded_at)
            received_delta = (now - ambient_latest.recorded_at).seconds
            data['ambient_status'] = self.status_for_time(received_delta, 600)

        data['red1'] = ambient_data.filter(device_name="Red 1")
        data['red2'] = ambient_data.filter(device_name="Red 2")
        data['red3'] = ambient_data.filter(device_name="Red 3")
        return render(self.request, self.template_name, data)

# Export - Tidy up one day, but not today

class CSVExport(TemplateView):
    def get(self, request):
        response = HttpResponse(content_type="text/csv")
        filename = "mk-green-roof"
        response.write(codecs.BOM_UTF8)

        from_date = parse_date(request.GET.get('from'))
        to_date = parse_date(request.GET.get('to'))

        from_datetime = timezone.make_aware(datetime(from_date.year,from_date.month,from_date.day))
        to_datetime = timezone.make_aware((datetime(to_date.year,to_date.month,to_date.day) + timedelta(days=1)) - timedelta(seconds=1))

        date_range = [from_datetime, to_datetime]
        if to_date < from_date:
            date_range.reverse()

        if request.GET.get('data') == "experiment":
            filename = filename + "-experiment-data"
            experiment_data = ExperimentData.objects.filter(recieved_at__range=date_range)
            header = [
                'recieved_at',
                'green_roof_top',
                'green_roof_middle',
                'green_roof_bottom',
                'surface_heat_difference',
                'bottom_difference',
                'control_top',
                'control_bottom',
                'moist_s0',
                'moist_s1',
            ]
            writer = csv.DictWriter(response, fieldnames=header)
            writer.writeheader()
            for d in experiment_data:
                writer.writerow({
                    'recieved_at':d.recieved_at,
                    'green_roof_top':d.green_roof_top,
                    'green_roof_middle':d.green_roof_middle,
                    'green_roof_bottom':d.green_roof_bottom,
                    'surface_heat_difference':d.surface_heat_difference,
                    'bottom_difference':d.bottom_difference,
                    'control_top':d.control_top,
                    'control_bottom':d.control_bottom,
                    'moist_s0':d.moist_s0,
                    'moist_s1':d.moist_s1,
                })
        else:
            filename = filename + "-ambient-data"
            ambient_data = AmbientData.objects.filter(recorded_at__range=[from_date, to_date])
            header = [
                'recorded_at',
                'device_name',
                'temperature',
                'humidity',
                'illumination',
            ]
            writer = csv.DictWriter(response, fieldnames=header)
            writer.writeheader()
            for d in ambient_data:
                writer.writerow({
                    'recorded_at':d.recorded_at,
                    'device_name':d.device_name,
                    'temperature':d.temperature,
                    'humidity':d.humidity,
                    'illumination':d.illumination,
                })

        filename = filename + "-"+str(from_date)+"-"+str(to_date)+".csv"
        response["Content-Disposition"] = 'attachment; filename="'+filename+'"'
        return response
